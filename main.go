package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/url"
	"os"

	"github.com/miekg/dns"
	"github.com/projectdiscovery/cdncheck"
)

var HOST string

func main() {
	flag.StringVar(&HOST, "target", "", "IP or URL")
	flag.Parse()

	if HOST == "" {
		usage()
	}

	result, err := checkDomainIP(HOST)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println(result)
}

func usage() {
	fmt.Println("usage: " + os.Args[0] + " -target <IP or URL>")
	os.Exit(1)
}

func checkDomainIP(host string) (string, error) {
	fmt.Println("[*] Target: " + host)
	if checkIP(host) {
		resultado, _ := scanCDN(host)
		fmt.Println("\t" + resultado)
	} else if checkDomain(host) {
		domain := obtainDomain(host)
		list_ip := dnsHostA(domain)
		for _, ip := range list_ip {
			resultado, _ := scanCDN(ip)
			fmt.Println("\t" + resultado)
		}
	}
	return "", nil
}

func checkIP(ip string) bool {
	parsedIP := net.ParseIP(ip)
	if parsedIP != nil && parsedIP.To4() != nil {
		return true
	} else {
		return false
	}
}

func checkDomain(s string) bool {
	u, err := url.Parse(s)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	} else {
		return true
	}
}

func obtainDomain(s string) string {
	u, err := url.Parse(s)
	if err != nil {
		log.Println(err)
	}
	return u.Host
}

func dnsHostA(domain string) []string {
	var hosts []string
	c := dns.Client{}
	m := dns.Msg{}
	m.SetQuestion(dns.Fqdn(domain), dns.TypeA)

	r, _, err := c.Exchange(&m, "1.1.1.1:53")
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
		os.Exit(1)
	}

	if len(r.Answer) == 0 {
		fmt.Println("No A records found.")
		os.Exit(1)
	} else {
		for _, ans := range r.Answer {
			if a, ok := ans.(*dns.A); ok {
				hosts = append(hosts, a.A.String())
			}
		}
	}
	return hosts
}

func scanCDN(host string) (string, error) {
	client, err := cdncheck.NewWithCache()
	if err != nil {
		return "", err
	}
	if found, result, err := client.Check(net.ParseIP(host)); found && err == nil {
		return "+ " + host + ": " + result, nil
	}

	return "- " + host + ": CDN not found", err
}
